FROM purban/centos7_java8

MAINTAINER Paul Urban

RUN yum install -y unzip wget && yum clean all \ 
    && wget https://services.gradle.org/distributions/gradle-2.6-bin.zip -P /tmp \
    && unzip -q /tmp/gradle-2.6-bin.zip -d /opt \
    && rm -rf /tmp/gradle-2.6-bin.zip \
    && mv /opt/gradle-2.6 /opt/gradle

ONBUILD ENV SRC shippable/buildoutput
ONBUILD ADD $SRC /opt/app
ONBUILD WORKDIR /opt/app

ENTRYPOINT [ "/opt/gradle/bin/gradle"] 

CMD [ "-v" ]
